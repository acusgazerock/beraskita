import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from '../components/AppBar/index';
import Grid from 'material-ui/Grid';
import withRoot from '../utils/withRoot';
import CardChart from '../components/CardChart/index';
import CardRice from '../components/CardRice/index';
import CardCompare from '../components/CardCompare/index';
import CardForm from '../components/CardForm/index';
import RankToko from '../components/RankToko/index';
import axios from 'axios';
import moment from 'moment';

const styles = theme => ({
  root: {
    width: '100%',
  },
  content: {
    backgroundImage: 'linear-gradient(to top, #3bcfb3, #8dd67a)',
    height: 'calc(100vh - 64px)',
    zIndex: 1,
    overflow: 'hidden',
    position: 'inherit',
    display: 'flex',
    width: '100%',
  },
  contentInner: {
    padding: theme.spacing.unit * 2,
    flex: 1,
    overflowY: 'auto'
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },

});

class SimpleAppBar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      auth : { username: 'username', password: 'password' },
      stock_type: [
        {
          "name": "Stock",
          "id_stock_type": 1
        },
        {
          "name": "Konsumsi",
          "id_stock_type": 2
        }
      ],
      order: [
        {
          text: "Tertinggi",
          val: "max"
        }, {
          text: "Terendah",
          val: "min"
        }
      ],
      jenis_beras: [],
      info_perbandingan: {},
      info_graph: {},
      info_rank_toko: [],
      info_rank_daerah: [],
      fistDate: '01/04/2017',
      lastDate: '12/4/2018',
      fstDate: '',
      productId: ''
    };
    this.handleChangeFirstDate = this.handleChangeFirstDate.bind(this);
    this.initInfoPerbandingan = this.initInfoPerbandingan.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.initInfoRankToko = this.initInfoRankToko.bind(this);
    this.initInfoRankDaerah = this.initInfoRankDaerah.bind(this);
  }

  handleChangeFirstDate = () => event => {
    let Dt = moment(event.target.value, "YYYY-MM-DD").format("DD/MM/YYYY");
    this.setState({ fistDate: Dt });
    // alert(this.state.fistDate)
    // this.initInfoPerbandingan(this.state.auth);
  };

  handleChangeLastDate = () => event => {
    const Dt = moment(event.target.value, "YYYY-MM-DD").format("DD/MM/YYYY");
    this.setState({
      lastDate: Dt,
    });
    // this.initInfoPerbandingan;
  };

  componentDidMount() {
    const auth = { username: 'username', password: 'password' }
    this.initJenisBeras(auth);
    this.initStockType(auth);
    this.handleSearch();
  }

  initJenisBeras(auth) {
    // const _this = this;
    axios.get('http://dashboard.bulir.id:8080/beraskita-core-webservice/products/list', {
      header: {
        'Content-Type': 'aplication/json'
      },
      auth
    }).then(res => {
      this.setState({ jenis_beras: res.data });
      // alert(JSON.stringify(this.state.jenis_beras))
    })
      .catch((error) => {
        console.log(error)
      })
  }

  initStockType(auth) {
    axios.get('http://dashboard.bulir.id:8080/beraskita-core-webservice/stock-types/list', {
      header: {
        'Content-Type': 'aplication/json'
      },
      auth
    }).then(res => {
      this.setState({ stock_type: res.data });
    })
      .catch((error) => {
        console.log(error)
      })
  }

  initInfoPerbandingan(auth) {
    axios.get('http://dashboard.bulir.id:8080/beraskita-core-webservice/user-stocks/total-stock?firstDate=' + this.state.fistDate + '&lastDate=' + this.state.lastDate, {
      header: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
      },
      auth
    }).then(res => {
      this.setState({ info_perbandingan: res.data });
    })
      .catch((error) => {
        console.log(error)
      })
  }

  initInfoRankToko(auth) {
    const productId = '8994209000010';

    axios.get('http://dashboard.bulir.id:8080/beraskita-core-webservice/user-stocks/rank?firstDate='+this.state.fistDate+'&lastDate='+this.state.lastDate+'&productId='+productId+'&stockTypeId=2&sort=max', {
      header: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
      },
      auth
    }).then(res => {
      this.setState({ info_rank_toko: res.data });
    })
      .catch((error) => {
        console.log(error)
      })
  }

  initInfoRankDaerah(auth) {
    const productId = '8994209000010';

    axios.get('http://dashboard.bulir.id:8080/beraskita-core-webservice/user-stocks/rank-by-area?firstDate='+this.state.fistDate+'&lastDate='+this.state.lastDate+'&productId='+productId+'&stockTypeId=2&sort=max', {
      header: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
      },
      auth
    }).then(res => {
      this.setState({ info_rank_daerah: res.data });
    })
      .catch((error) => {
        console.log(error)
      })
  }

  handleSearch() {
    this.initInfoPerbandingan(this.state.auth);
    this.initInfoRankToko(this.state.auth);
    this.initInfoRankDaerah(this.state.auth);
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <AppBar />
        <div className={classes.content}>
          <div className={classes.contentInner}>
            <Grid container spacing={16}>
              <Grid item xs={6} container spacing={8}>
                <Grid item xs={6}>
                  <CardForm title="Tanggal Laporan" style={{ alignItems: "stretch" }} firstDate={this.state.fistDate} lastDate={this.state.lastDate} handleChangeFirstDate={this.handleChangeFirstDate} handleChangeLastDate={this.handleChangeLastDate} onClick={this.handleSearch} />
                  <br />
                  <CardRice stock={this.state.info_perbandingan.total_stock} consumption={this.state.info_perbandingan.total_consumption} style={{ alignItems: "stretch" }} />
                </Grid>
                <Grid item xs={6}>
                  <CardCompare title="Perbandingan Konsumsi Jenis Beras Nasional" data={this.state.info_perbandingan} />
                </Grid>
              </Grid>
              <Grid item xs={6}>
                <CardChart title="Perbandingan Konsumsi Jenis Beras Nasional" jenis_beras={this.state.jenis_beras} stock_type={this.state.stock_type} />
              </Grid>
            </Grid>

            <Grid container xs={12} spacing={16}>
              <Grid item xs={6} container >
                <RankToko
                  title="Ranking Performansi Toko" 
                  jenis_beras={this.state.jenis_beras}
                  stock_type={this.state.stock_type}
                  order={this.state.order}
                  data={this.state.info_rank_toko}
                  type="toko"
                />
              </Grid>
              <Grid item xs={6}>
                <RankToko
                  title="Ranking Performansi Daerah"
                  jenis_beras={this.state.jenis_beras}
                  stock_type={this.state.stock_type}
                  order={this.state.order}
                  data={this.state.info_rank_daerah}
                  type="daerah"
                />
              </Grid>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

SimpleAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withRoot(withStyles(styles)(SimpleAppBar));
