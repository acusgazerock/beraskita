import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import withRedux from 'next-redux-wrapper';

import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';

import LoginForm from '../components/Login/Form'
import initStore from '../store/configureStore';
import axios from 'axios';
import withRoot from '../utils/withRoot';

const redirect = route => {
  window.location.href = route;
};

const styles = theme => ({
  root: {
    flexGrow: 1, height: "100vh", backgroundImage: 'linear-gradient(to top, #3bcfb3, #8dd67a)',

  }, paper: {
    padding: theme.spacing.unit * 2, height: '100%',
  }, control: {
    padding: theme.spacing.unit * 2,
  },
});

class Login extends React.Component {
  handleSubmit(auth) {
    axios.post('http://dashboard.bulir.id:8080/beraskita-usermanagement-webservice/admin/login', auth, {
      header: {
        'Content-Type': 'aplication/json'
      },
      auth
    }).then(res => {
      redirect('/dashboard');

    })
    .catch((error) => {
      console.log(error)
    })
  }

  render() {
    const { classes } = this.props;
    return (
      <Grid
        container
        className={classes.root}
        justify="center"
        alignItems="center"
      >
        <Grid >
          <LoginForm   onSubmit={this.handleSubmit} />
        </Grid>
      </Grid>
    )
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default  withRoot(
  withStyles(styles)(
    withRedux(initStore)(
      Login
    )
  )
);