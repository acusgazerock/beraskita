import React from 'react';
import { Field, reduxForm } from 'redux-form';
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'
import Card, { CardHeader, CardMedia, CardContent, CardActions } from 'material-ui/Card';
import { withStyles } from 'material-ui/styles';

const validate = values => {
  const errors = {};
  const requiredFields = ['username', 'password'];
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  });
  return errors
};
const styles = theme => ({
  card: {
    width: 400,
  }, actions: {
    display: 'flex',
  },
});
const renderTextField = ({ input, label, type, meta: { touched, error }, ...custom }) => (
  <TextField
    margin="normal"
    label={label}
    type={type}
    error={(touched && error) ? true : false}
    helperText={(touched && error) ? error : ''}
    {...input}
    {...custom}
  />

);
const LoginForm = props => {
  const { handleSubmit, pristine, reset, submitting, classes } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Card className={classes.card}>
        <CardMedia
          className={classes.cover}
          image="/static/image.svg"
        />
        <CardContent>
          <Field
            name="username"
            component={renderTextField}
            label="Username/Email"
            placeholder="Username/Email"
            fullWidth
          />
          <Field
            name="password"
            component={renderTextField}
            placeholder="Password"
            label="Password"
            type="password"
            fullWidth
          />
        </CardContent>
        <CardActions className={classes.actions}>
          <Button type="submit" disabled={pristine || submitting} color="primary" variant="raised" fullWidth>Masuk</Button>
          {/*<Button type="button" disabled={pristine || submitting} onClick={reset}>*/}
          {/*Clear Values*/}
          {/*</Button>*/}
        </CardActions>
      </Card>


    </form>
  );
};

export default reduxForm({
  form: 'loginForm', // a unique identifier for this form
  validate
})(withStyles(styles)(LoginForm));
