import React from 'react';
import PropTypes from 'prop-types';
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';

const styles = {
  card: {
    display: 'flex',
    height: 418,
  },
  cover: {
    width: 35,
    height: 25,
    padding: 25,
    margin: '20px 0 15px 15px',
    display: 'flex',
    backgroundSize: 'contain',
  },
  content: {
    flex: '1 1 auto',
    textAlign: 'left',
  },
  value: {
    color: '#000000',
  },
  progress: {
    background: 'none',
    height: 35
  },
  title: {
    textAlign: 'center',
  },
  konsumsi: {
    height: 1,
    verticalAlign: 'center',
    color: '#ffffff',
    padding: 4,
    fontSize: '0.7em',
    fontWeight: '400',
  }
};

class CardCompare extends React.Component {
  constructor() {
    super();

    this.state = {
      data: [
        { title: 'Beras Premium 20 Kg', value: 0, color: '#53d4bc' },
        { title: 'Beras Premium 10 Kg', value: 0, color: '#4aa191' },
        { title: 'Beras Premium 5 Kg', value: 0, color: '#93d98f' },
        { title: 'Beras Medium 20 Kg', value: 0, color: '#65c75f' },
        { title: 'Beras Medium 10 Kg', value: 0, color: '#93d98f' },
        { title: 'Beras Medium 5 Kg', value: 0, color: '#4aa191' },
      ],
      maxValue: 0
    };
  }

  _setValues() {
    const { data } = this.props;
    const stateData = this.state.data;
    let maxValue = 0;

    stateData.map(item => {
      switch (item.title) {
        case 'Beras Premium 20 Kg':
          item.value = data.premium_rice20_consumption;
          break;
        case 'Beras Premium 10 Kg':
          item.value = data.premium_rice10_consumption;
          break;
        case 'Beras Premium 5 Kg':
          item.value = data.premium_rice5_consumption;
          break;
        case 'Beras Medium 20 Kg':
          item.value = data.medium_rice20_consumption;
          break;
        case 'Beras Medium 10 Kg':
          item.value = data.medium_rice10_consumption;
          break;
        case 'Beras Medium 5 Kg':
          item.value = data.medium_rice5_consumption;
          break;
      }

      maxValue = maxValue < item.value ? item.value : maxValue;
      return item;
    });

    return maxValue;
  }

  render() {
    const { title, classes, data } = this.props;

    if (!data) {
      return (<Typography>Loading..</Typography>)
    } else {
      const maxValue = this._setValues();
      return (
        <Card className={classes.card}>
          <CardContent className={classes.content}>
            <Typography variant="body2" gutterBottom className={classes.title}>
              {title}
            </Typography>
            <br />
            {this.state.data.map((n, i) => (
              <div key={i}>
                <Typography variant="body1">
                  <br />
                  {`${n.title} (${n.value} Kg)`}
                </Typography>
                <Typography variant="body1" style={{ width: (n.value * 100 / maxValue) + '%', backgroundColor: n.color }} className={classes.konsumsi} />
              </div>
            )
            )}
          </CardContent>
        </Card>
      );
    }
  }
}

CardCompare.propTypes = {
  title: PropTypes.string,
  data: PropTypes.object,
  classes: PropTypes.object
};

export default withStyles(styles)(CardCompare);