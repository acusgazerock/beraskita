import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

const styles = {
  bar: {
    color: '#ffffff',
    padding: '20px 15px',
    position: 'relative',
    fontSize: '14px',
    boxShadow: '0 12px 20px -10px rgba(255, 255, 255, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(255, 255, 255, 0.2)',
    fontWeight: '300',
    lineHeight: '20px',
    borderRadius: '3px',
    marginBottom: '20px',
    backgroundColor: 'white',
  }
};

class Component extends React.Component {  
  render() {
    const { title, percentage, classes } = this.props;
    return (
      <div className={classes.bar}>
        {title}
        {percentage}
        title
      </div>
    );
  }
}

Component.propTypes = {
  title: PropTypes.string,
  percentage: PropTypes.int
};

export default withStyles(styles)(Component);