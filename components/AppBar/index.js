import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/Button';
import PowerSettingsNew from 'material-ui-icons/PowerSettingsNew';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  flex: {
    flex : 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  pict: {
    width: 100,
  }
});

function SimpleAppBar(props) {
  const { classes } = props;
  return (
    <AppBar position="static" color="white">
      <Toolbar>

        <Typography variant="title" color="inherit" className={classes.flex}>
          <img src="/static/mask.svg" className={classes.pict} />
        </Typography>
        {/* <IconButton color="inherit">Login</IconButton> */}
        {/* <IconButton color="inherit">Map</IconButton> */}
        <IconButton>
          <PowerSettingsNew />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
}

SimpleAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleAppBar);