const styles = {
  card: {
    display: 'flex'
  },
  filterCharts: {
    float: 'right',
    display: 'flex',
    marginRight: 15,
  },
  tableColumn: {
    padding: 3,
  },
  label: {
    height: 13,
    verticalAlign: 'center',
    color: '#ffffff',
    marginTop: 7,
    padding: 4,
    fontSize: '0.7em',
    fontWeight: '400',
  }
};

export default styles;