import React from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Card, { CardContent} from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import moment from 'moment';
const styles = {
  card: {
    display: 'flex',
    height: 200,
  },
  lineForm: {
    display: 'flex',
  },
  formDate: {
    width:'100%',
  },
  buttonSearch: {
    marginTop: 25,
    marginLeft: 15,
    width: '90%',
    backgroundColor: '#509470',
    color: '#ffffff',
    borderRadius: 20,
  },
  title: {
    textAlign: 'center',
  },
};

class CardForm extends React.Component {
  render() {
    const { title, classes,firstDate, lastDate, handleChangeFirstDate, handleChangeLastDate, onClick } = this.props;
    const fDt = moment(firstDate, "DD-MM-YYYY").format("YYYY-MM-DD");
    const lDt = moment(lastDate, "DD-MM-YYYY").format("YYYY-MM-DD");

    return (
      <Card>
        <br />
        <Typography variant="body2" gutterBottom className={classes.title}>
          {title}
        </Typography>
        <br />
        <CardContent >
          <div className={classes.lineForm}>
            <TextField
              id="firstDate"
              type="date"
              defaultValue={fDt}
              onChange={handleChangeFirstDate(fDt)}
              className={classes.formDate}
            />
            <Typography variant="body2" gutterBottom noWrap className={classes.title}>
              s/d
            </Typography>
            <TextField
              id="lastDate"
              type="date"
              defaultValue={lDt}
              onChange={handleChangeLastDate(lDt)}
              className={classes.formDate}
            />
          </div>
          <Button color="primary" className={classes.buttonSearch} onClick={onClick}>Cari</Button>
        </CardContent>
      </Card>
    );
  }
}
CardForm.propTypes = {
  title: PropTypes.string,
};

export default withStyles(styles)(CardForm);